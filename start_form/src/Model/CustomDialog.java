/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package Model;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 *
 * @author Francky
 */
public class CustomDialog {
    public static void showDialog(String title, String text) {
        JOptionPane jOptionPane = new JOptionPane();
        jOptionPane.setMessage(text);
        JDialog dialog = jOptionPane.createDialog(title);
        dialog.setAlwaysOnTop(true);
        dialog.setVisible(true);
    }
}
