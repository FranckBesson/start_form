/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package Model;

/**
 *
 * @author Francky
 */
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Mail {
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    
    public static void sendMail(String textMessage) throws FileNotFoundException, IOException, MessagingException {
        
        //For get the mail properties
        Properties prop = new Properties();
        FileReader reader = new FileReader("ressources\\config.properties");
        prop.load(reader);
        
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(prop.getProperty("idMailSender"),prop.getProperty("passwordMailSender"));
                    }
                });
        
        Message message = new MimeMessage(session);
        //message.setFrom(new InternetAddress(prop.getProperty("mailSender")));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(prop.getProperty("mailReceiver")));
        message.setSubject(prop.getProperty("mailSubject"));
        message.setText(textMessage);
        
        Transport.send(message);
        
    }
    
    public static String formatTextMessage(String name, String firstName, String stringClass, String teacher, String probleMeet){
        
        
        
        String hostname = "Unknown";
        
        try
        {
            InetAddress addr;
            addr = InetAddress.getLocalHost();
            hostname = addr.getHostName();
        }
        catch (UnknownHostException ex)
        {
            hostname = "Unknown";
        }
        
        //Mail send
        return "Bonjour,\n\n"
                + "L'élève "+name+" "+firstName+" ("+stringClass+") dont le professeur est "+teacher+" a rencontré ce problème :\n"
                + "\""+probleMeet+"\"\n\n"
                + "Nom de l'utilisateur : "+ System.getProperty("user.name") +"\n"
                + "Nom du poste : "+ hostname +"\n\n"
                + "Cordialement\n\n"
                + "Start Form";
    }
    
    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }
}