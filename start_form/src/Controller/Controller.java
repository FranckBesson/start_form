/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package Controller;

import Model.Mail;
import View.View;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.mail.MessagingException;

/**
 *
 * @author Francky
 */
public class Controller {
    
    private View view;
    
    public Controller(View view){
        this.view = view;
    }
    
    //Simple mail sender
    public void sendMail(String name, String firstName, String stringClass, String teacher, String probleMeet) throws IOException, FileNotFoundException, MessagingException {
        Mail.sendMail(Mail.formatTextMessage(name, firstName, stringClass, teacher, probleMeet));
    }
    
    public boolean validateReceiverMail(String emailStr){
        return Mail.validate(emailStr);
    }

}
