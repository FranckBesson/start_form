/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package Exception;

/**
 *
 * @author Francky
 */
public class EmptyStringException extends Exception{
    
    String message;
    
    public EmptyStringException(String message){
        this.message=message;
    }
    
    @Override
    public String getMessage(){
        return this.message;
    }
    
}
