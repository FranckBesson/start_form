@echo off

:: BatchGotAdmin
:-------------------------------------
REM  --> Check for permissions
    IF "%PROCESSOR_ARCHITECTURE%" EQU "amd64" (
>nul 2>&1 "%SYSTEMROOT%\SysWOW64\cacls.exe" "%SYSTEMROOT%\SysWOW64\config\system"
) ELSE (
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
)

REM --> If error flag set, we do not have admin.
if '%errorlevel%' NEQ '0' (
    echo Requesting administrative privileges...
    goto UACPrompt
) else ( goto gotAdmin )

:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"=""
    echo UAC.ShellExecute "cmd.exe", "/c ""%~s0"" %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"

    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
    pushd "%CD%"
    CD /D "%~dp0"
:-------------------------------------- 

cd > temp.txt
set /p currentpath=< temp.txt

set javapath=%currentpath%\start_form.jar
echo cd %currentpath%\ > start_form.bat
echo java -jar start_form.jar >> start_form.bat

echo cd %currentpath%\ > start_form_launcher.bat
echo wscript.exe "invisible.vbs" "start_form.bat" >> start_form_launcher.bat

set allpath=%currentpath%\start_form_launcher.bat

SCHTASKS /CREATE /TN "start_from" /TR %allpath% /SC ONLOGON 2>> "log_install".log